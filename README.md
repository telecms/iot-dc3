<p align="center">
	<img src="./docs/images/dc3/logo-blue.png" width="400"><br>
  <a href='https://gitee.com/pnoker/iot-dc3/stargazers'><img src='https://gitee.com/pnoker/iot-dc3/badge/star.svg?theme=gvp ' alt='star'></img></a>
	<a href='https://gitee.com/pnoker/iot-dc3/members'><img src='https://gitee.com/pnoker/iot-dc3/badge/fork.svg?theme=gvp ' alt='fork'></img></a>
	<br>
	<a><img src="https://img.shields.io/badge/JDK-1.8-green.svg"></a>
	<a><img src="https://img.shields.io/badge/Spring Boot-2.6.2-blue.svg"></a>
	<a><img src="https://img.shields.io/badge/Spring Cloud-2021.0.0-blue.svg"></a>
	<a href="https://github.com/pnoker/iot-dc3/blob/master/LICENSE"><img src="https://img.shields.io/github/license/pnoker/iot-dc3 .svg"></a>	
	<br><strong>DC3 is an open source and distributed Internet of Things (IOT) platform based on Spring Cloud, which is used for rapid development and deployment of IoT device access projects. It is a complete set of IoT system solutions. </strong>
</p>

------


### 1 DC3 Architecture Design

![iot-dc3-architecture](./docs/images/dc3/architecture1.jpg)



#### DC3 module division, four-tier architecture

 * [x] Driver layer: `SDK` used to provide standard or private protocol to connect physical devices, responsible for data acquisition and command control of southbound equipment, based on `SDK` to realize rapid driver development;
 * [x] Data layer: responsible for the collection and storage of device data, and provide data management interface services;
 * [x] Management layer: It is used to provide microservice registration center, device command interface, device registration and association pairing, and data management center. It is the core part of all microservice interactions. It is responsible for the management of various configuration data and provides external interfaces. Serve;
 * [ ] Application layer (under development...): used to provide rule engine, data opening, task scheduling, alarm and message notification, log management, etc., with the ability to connect to third-party platforms.



#### DC3 function design, positioning target

 * [x] Scalable: a horizontally scalable platform built using the leading `Spring Cloud` open source technology;
 * [x] fault tolerance: no single point of failure weak, every node in the cluster is the same;
 * [x] Robust and efficient: a single server node can handle even hundreds of thousands of devices depending on the use case;
 * [x] Customizable: add new device protocol and register to service center;
 * [x] Cross-platform: use the `Java` environment for remote, distributed multi-platform deployment;
 * [x] Autonomous and controllable: private cloud, public cloud, edge deployment;
 * [X] Perfection: device quick access, registration, permission verification;
 * [ ] Security (under development...): data encryption transmission (mqtt driver has implemented data encryption transmission);
 * [x] Multi-tenancy: namespace, multi-tenancy;
 * [X] Cloud native: Kubernetes;
 * [x] Containerization: Docker.



### 2 DC3 Technical Implementation

The `DC3` platform is developed based on the `Spring Cloud` architecture, which is a collection of loosely coupled, open source microservices.
The microservice collection consists of four microservice layers and two enhanced basic system services, which provide a series of services from data collection in the physical domain to data processing in the information domain.

![iot-dc3-architecture](./docs/images/dc3/architecture2.jpg)



### 3 Project Documentation

> For details, please read the [`IOT DC3 Wiki (https://doc.dc3.site)`](https://doc.dc3.site) documentation
>
> **Including: installation configuration documentation, deployment documentation, project structure description, platform introduction, etc. **



### 4 Open Source Contributions

- `checkout` a new branch from the `master` branch (**NOTE**: *make sure the `master` code is up to date*)
- New branch naming format: `docs/username_description`, for example: `docs/pnoker_Add mqtt usage description`
- Edit documentation, code, and commit code on new branches
- Finally, `PR` is merged into the `develop` branch, waiting for the author to merge



### 5 Open Source Protocol

The `IOT DC3` open source platform follows the [Apache 2.0 license](https://www.apache.org/licenses/LICENSE-2.0.html). Commercial use is allowed, but be sure to keep the class author, Copyright information.